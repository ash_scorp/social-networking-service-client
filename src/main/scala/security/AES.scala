package security

/**
  * Created by mebin on 12/11/15 for Facebook-Client
  */

import java.security._
import java.security.spec.KeySpec
import javax.crypto.spec.{PBEKeySpec, SecretKeySpec, IvParameterSpec}
import javax.crypto.{SecretKeyFactory, Cipher, KeyGenerator, SecretKey}
import org.apache.commons.codec.binary.Base64
import java.util

import sun.misc.BASE64Encoder

object AES {

  val prng = new SecureRandom()

  def generateKey: String = {
    val keyGen = KeyGenerator.getInstance("AES")
    keyGen.init(128)
    val secretKey = keyGen.generateKey()
    Base64.encodeBase64String(secretKey.getEncoded)
  }

  def encrypt(key: String, strDataToEncrypt: String): (String, String) = {
    val keyByte = Base64.decodeBase64(key)
    val keyLength = 128
    val iv = new Array[Byte](16)
    prng.nextBytes(iv)
    val aesCipherForEncryption = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    val keyGen = KeyGenerator.getInstance("AES")
    keyGen.init(128)
    val secretKey = new SecretKeySpec(keyByte, "AES")
    aesCipherForEncryption.init(Cipher.ENCRYPT_MODE, secretKey,
      new IvParameterSpec(iv))
    val byteDataToEncrypt = strDataToEncrypt.getBytes()
    val byteCipherText = aesCipherForEncryption
      .doFinal(byteDataToEncrypt)
    val strCipherText = Base64.encodeBase64String(byteCipherText)
    (strCipherText, Base64.encodeBase64String(iv))
  }

  def decrypt(key: Array[Byte], encryptedText: String, iv: String): String = {
    val aesCipherForDecryption = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    val decodedIv = Base64.decodeBase64(iv)
    val decodedKeyByte = Base64.decodeBase64(key)
    val decodecEncText = Base64.decodeBase64(encryptedText)
    val secretKey = new SecretKeySpec(decodedKeyByte, "AES")
    aesCipherForDecryption.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(decodedIv))
    val byteDecryptedText = aesCipherForDecryption
      .doFinal(decodecEncText)
    Base64.encodeBase64String(byteDecryptedText)
  }

  def main(args: Array[String]) {

    val serverPublicKey: KeyPair = RSA.loadKeyPair("C://Users//Ashish//Documents", "RSA")

    val key = generateKey

    val (encText, iv) = encrypt(key, "{id:a88a564a-d699-4768-b751-e920a2cf384e,message:75853 Independence Junction,created_time:6/27/2015}")

    var aesEncrypt = RSA.encrypt(key, serverPublicKey.getPublic)

    var desEncrypt = RSA.decrypt(aesEncrypt, serverPublicKey.getPrivate)

    println("The length after rsa decryption is " + Base64.decodeBase64(desEncrypt).length)

    println(new String(Base64.decodeBase64(decrypt(Base64.decodeBase64(desEncrypt), encText, iv))))
  }
}

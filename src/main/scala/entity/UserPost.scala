package entity

import spray.json.DefaultJsonProtocol

/**
 * Created by mebin on 12/14/15 for Facebook-client_1
 */
case class UserPost(
                 id: String, // The id of the post
                 caption: Option[String], //The caption of the link in the post
                 created_time: Option[String], //The created time
                 description: Option[String], // A description of a link in the post.
                 link: Option[String], //The link attached to this post
                 message: Option[String], //the status message of this post
                 location: Option[String], // Any location attached to the post
                 shares: Option[String], // The shares count of this post.
                 updated_time: Option[String], // The time of the last change to this post, or the comments on it. For a post about a life event, this will be the date and time of the life event
                 replies: Option[List[String]] // The replies to the post or comment to it!!
                 )

object UserPostJsonImplicits extends DefaultJsonProtocol {

  implicit val userPostJsonImplicit = jsonFormat10(UserPost)
}


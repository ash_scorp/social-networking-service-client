package entity

import spray.json.DefaultJsonProtocol

/**
  * Created by Ashish on 12/15/2015.
  */
case class Picture (name: String, //The album ID
                    id: String, // The user provided caption given to this photo.
                    description: Option[String], // The description of the album
                    album: Option[String], //The album the photo is in
                    created_time: Option[String], //The time the photo was published
                    can_delete: Option[String], // A boolean indicating if the viewer can delete the photo
                    can_tag: Option[String], // A boolean indicating if the viewer can tag the photo
                    from: Option[String], //The profile (user or page) that uploaded this photo
                    link: Option[String], //A link to the photo on Facebook
                    location: Option[String], //Location associated with the photo, if any
                    //picture:Option[String], // A link to the 100px wide representing the photo

                    //Edges
                    likes: Option[List[String]], //People who like this
                    sharedPosts: Option[String], //People who share this
                    Comments: Option[String] //Comments made on it by self or other users
                   )
object PictureJsonImplicits extends DefaultJsonProtocol {

  implicit val impPicture = jsonFormat13(Picture)
}
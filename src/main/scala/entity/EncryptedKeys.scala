package entity

/**
 * Created by mebin on 12/13/15 for Facebook-client_1
 */
case class EncryptedKeys(encryptedAESKeyOfUser: String, encryptedAESKeyOfFriend: String)

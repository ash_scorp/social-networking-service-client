package entity

import spray.json.DefaultJsonProtocol

/**
 * Created by mebin on 12/13/15 for Facebook-client_1
 */
case class FriendList(id: Int, name: String)

object FriendJsonProtocol extends DefaultJsonProtocol {
  implicit val frndId = jsonFormat2(FriendList)
}


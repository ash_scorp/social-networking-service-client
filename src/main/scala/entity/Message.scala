package entity

/**
  * Created by mebin on 12/14/15 for Facebook-client_1
  */
trait Message

case class SendFriendRequest(msg: String) extends Message

case class AcceptFriendRequest(msg: String) extends Message

case class Done(msg: String) extends Message

case class AddPost(msg: String) extends Message

case class ReadPost(msg: String) extends Message

case class AddPic(msg: String) extends Message

case class ReadPic(msg: String) extends Message
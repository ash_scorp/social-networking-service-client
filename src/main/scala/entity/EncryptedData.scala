package entity

import spray.json.DefaultJsonProtocol

/**
 * Created by mebin on 12/14/15 for Facebook-client_1
 */
case class EncryptedData(entityId:String, iv:String, encryptedEntity:String)

object EncryptedDataJsonImplicits extends DefaultJsonProtocol{
  implicit val encDataId = jsonFormat3(EncryptedData)
}

import java.io.{FileInputStream, ObjectInputStream}
import java.security.{KeyPair, MessageDigest, PublicKey}

import akka.actor._
import akka.event.Logging
import akka.io.IO
import akka.pattern.ask
import akka.routing.RoundRobinPool
import entity.UserPost
import entity._
import security.RSA
import spray.can.Http
import spray.client.pipelining._
import spray.util._

import scala.collection.mutable
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.Source

//Json Parser

import net.liftweb.json.DefaultFormats
import net.liftweb.json.JsonParser._
import spray.json._
import akka.util.Timeout

object Main extends App {
  // we need an ActorSystem to host our application in
  //Servers public key
  val loadedKeyPair: KeyPair = RSA.loadKeyPair("C://Users//Ashish//Documents//", "RSA")
  val serverPublicKey = loadedKeyPair.getPublic

  implicit val system = ActorSystem("facebook-simple-spray-client")
  val userActorList: scala.collection.mutable.MutableList[ActorRef] = scala.collection.mutable.MutableList[ActorRef]()
  val ticker = system.actorOf(Props[Ticker], name = "ticker")

  import system.dispatcher

  val log = Logging(system, getClass)
  log.info("Requesting facebook info...")
  val userFileName = "src/main/resources/user.json"
  val userLines = Source.fromFile(userFileName).mkString
  val postFileName = "src/main/resources/post.json"
  var postLines = Source.fromFile(postFileName).mkString
  val photosFileName = "src/main/resources/photos.json"
  val photosLines = Source.fromFile(photosFileName).mkString

  implicit val formats = DefaultFormats
  val pipeline = sendReceive ~> unmarshal[String]
  val userList = parse(userLines).extract[List[User]]
  val postList = parse(postLines).extract[List[UserPost]]
  val photosList = parse(photosLines).extract[List[Picture]]

  val userIdList: List[String] = List()
  private val userParentPath: String = "http://localhost:8080/user/"
  println("--------------------------Starting to Add the users to the system---------------------")
  println(s"No of users are ${userList.size}")
  for (user <- userList) {
    val newUser: User = new User(user.name, user.id, user.userName, MessageDigest.getInstance("SHA-1").digest(user.password.getBytes).toString(), user.email, user.birthday, user.frnds, null)
    val mockUserActor = system.actorOf(Props[MockUserActor], newUser.id)

    userActorList += mockUserActor
    implicit val timeout = Timeout(8 seconds)
    val f = mockUserActor ? newUser
    Await.result(f, timeout.duration)
  }
  println("--------------------------Finished adding the users to the system---------------------")

  println("Get all user ids from the server")
  val nresponseFuture = pipeline {
    Get(userParentPath + "allIds")
  }

  val userResult = Await.result(nresponseFuture, 80 second) match {
    case s: String => s
  }
  val userJsonList = userResult.parseJson
  println("--------------------------------------User finished -------------------------------------------")

  activeUsers


  def activeUsers: Unit = {
    //make post at higher frequency
    val r = scala.util.Random
    implicit val timeout = Timeout(5 seconds)

//    val f = userActorList(0) ? SendFriendRequest
//    Await.result(f, 30 seconds)
//
//    val g = userActorList(1) ? AcceptFriendRequest
//    Await.result(g, 30 seconds)

    if (userActorList != null) {
      for (i <- 1 to 20) {
        val f = userActorList(r.nextInt(userActorList.size)) ? SendFriendRequest
        Await.result(f, 30 seconds)
      }
    }
    if (userActorList != null) {
      for (i <- 1 to 20) {
        val f = userActorList(r.nextInt(userActorList.size)) ? AcceptFriendRequest
        Await.result(f, 2 seconds)
      }
    }

    if (userActorList != null) {
      for (i <- 1 to 20) {
        val f = userActorList(r.nextInt(userActorList.size)) ? AddPost
        Await.result(f, 2 seconds)
      }
    }

    if (userActorList != null) {
      for (i <- 1 to 20) {
        val f = userActorList(r.nextInt(userActorList.size)) ? ReadPost
        Await.result(f, 10 seconds)
      }
    }

    if (userActorList != null) {
      for (i <- 1 to 20) {
        val f = userActorList(r.nextInt(userActorList.size)) ? AddPic
        Await.result(f, 2 seconds)
      }
    }

    if (userActorList != null) {
      for (i <- 1 to 20) {
        val f = userActorList(r.nextInt(userActorList.size)) ? ReadPic
        Await.result(f, 10 seconds)
      }
    }


  }


  def shutdown(): Unit = {
    IO(Http).ask(Http.CloseAll)(1.second).await
    println("Yes i got shutdown")
    system.shutdown()
    System.exit(0)
  }
}

class Ticker extends Actor {
  implicit val system = ActorSystem("facebook-simple-spray-client")

  override def receive: Receive = {
    case "Tick" => println("Ticker")
  }
}
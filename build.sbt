name := "Facebook-client_1"

version := "0.1"

scalaVersion := "2.11.6"

libraryDependencies ++= {
  val akkaV = "2.3.9"
  val sprayV = "1.3.3"
  Seq(
    "io.spray" %% "spray-can" % sprayV,
    "io.spray" %% "spray-json" % "1.3.2",
    "io.spray" %% "spray-client" % sprayV,
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
    "net.liftweb" %% "lift-json" % "2.6+",
    "org.json4s" %% "json4s-native" % "3.3.0",
    "commons-codec" % "commons-codec" % "1.10"

  )
}